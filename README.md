# Jupyter Notebook Template

The idea is to have an all-rounder template for Data Analysis work using JupyterLab.

## References

- [Mito – Write Python/Pandas faster by editing a spreadsheet, in Jupyter](https://news.ycombinator.com/item?id=26377559)
- [On writing clean Jupyter notebooks](https://ploomber.io/posts/clean-nbs/)
- [Introducing JupyterDash](https://medium.com/plotly/introducing-jupyterdash-811f1f57c02e)
- [Voila – From notebooks to standalone web applications and dashboards](https://news.ycombinator.com/item?id=28364923)
